package pl.sda.language_detector;

import com.detectlanguage.DetectLanguage;
import com.detectlanguage.Result;
import com.detectlanguage.errors.APIError;

import java.io.*;
import java.util.List;
import java.util.Locale;

public class LanguageDetector {
    public static void detectLangugeFromFolder(){


        DetectLanguage.apiKey = "8948bdd0abc414c80d3354cae32c4095";
// Enable secure mode (SSL) if passing sensitive information
// DetectLanguage.ssl = true;


        FileReader fileReader = null;

        try {

            File folder = new File("src\\main\\resources\\");
            File[] listOfFiles = folder.listFiles();

            for (File file : listOfFiles) {
                if (file.isFile()) {
                    StringBuilder stringBuilder = new StringBuilder();
                    fileReader = new FileReader(file);
                    BufferedReader reader = new BufferedReader(fileReader);
                    String linia;


                    while ((linia = reader.readLine()) != null) {
                        stringBuilder.append(linia + " ");
                    }



                    List<Result> results = null;
                    try {
                        results = DetectLanguage.detect(stringBuilder.toString());
                    } catch (APIError apiError) {
                        apiError.printStackTrace();
                    }
                    Result result = results.get(0);
                    System.out.println("Language: " + new Locale(result.language).getDisplayLanguage());
                    System.out.println("Is reliable: " + result.isReliable);
                    System.out.println("Confidence: " + result.confidence);


                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            try {
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
